{$r lang_en.rc}

const
    monsters: array [0..MONSTERS_COUNT - 1] of TString = (
        'BAT',
        'RAT',
        'LIZARD',
        'SPIDER',
        'DOG',
        'SNAKE',
        'KRONK',
        'GNOME',
        'GOBLIN',
        'ZOMBIE',
        'WARG',
        'GHOST',
        'ELF',
        'WEREWOLF',
        'WITCH',
        'ORC',
        'GARGOYLE',
        'TROLL',
        'MAGE',
        'VAMPIRE',
        'NIGHTMARE',
        'MINOTAUR',
        'CYCLOPE',
        'GIANT',
        'HYDRA',
        'DEMON',
        'MEDUSA',
        'DRAGON',
        'DEVIL',
        'PHOENIX'
     );
    weapons: array [0..WEAPONS_COUNT - 1] of TString = (
        'FORK',
        'KNIFE',
        'RAZOR',
        'DAGGER',
        'SABER',
        'LANCE',
        'JAVELIN',
        'SCIMITAR',
        'AXE',
        'SWORD' 
    );
    treasures: array [0..TREASURES_COUNT - 1] of TString = (
        'COPPER',
        'SILVER',
        'GOLD',
        'PLATINUM',
        'DIAMONDS' 
    );
    items: array [0..ITEMS_COUNT - 1] of TString = (        
        'HAMMER',
        'LANTERN',
        'KEY',
        'PLANK',
        'FOOD',
        'DRINKS',
        'BANDAGE',
        'MEDKIT',
        'PASSWORD' 
    );
    itemSymbols: array [0..ITEMS_COUNT - 2] of char = ( 'H', 'L', 'K', 'P', 'F', 'D', 'B', 'M');
    passwords: array [0..PASSWORDS_COUNT - 1] of TString = ('FISH','EGGS','BYTE','TREE','CLAN','CULT','MOON','BULB','LASH');

const
    k_YES = byte('Y');
    k_NO = byte('N');
    k_LEFT = byte('L');
    k_RIGHT = byte('R');
    k_UP = byte('U');
    k_DOWN = byte('D');
    k_FIGHT = byte('F');
    k_RANSOM = byte('B');
    k_TAKE = byte('T');
    k_LEAVE = byte('L');
    k_REST = byte('R');
    k_MOVE = byte('M');
    k_BANDAGE = byte('B');
    k_MEDKIT = byte('M');
    s_PRESS_ANY: string = 'PRESS ANY KEY';
    s_WANT_MANUAL: string = ' DO YOU WANT TO READ A MANUAL (Y/N)?';
    s_ENERGY: string = 'ENERGY'*;
    s_TREASURE: string = 'TREASURE'*;
    s_MOVES: string = 'MOVES'*;
    s_WOUNDS: string = 'WOUNDS'*;
    s_ITEMS: string = 'ITEMS'*;
    s_WEAPON: string = 'YOUR WEAPON'*;
    s_ATTACK: string = 'ATTACK'*;
    s_FOUND: string = 'YOU HAVE FOUND ';
    s_TAKE: string = 'T'*'AKE';
    s_OR: string = ' OR ';
    s_LEAVE: string = 'L'*'EAVE';
    s_FOUND_PASS: string = 'YOU''VE FOUND PASSWORD: ';
    s_REMEMBER: string = 'REMEBER IT.';
    s_LEAVE_WHAT: string = 'WHAT DO YOU WANT TO DROP?   ';
    s_DONT_HAVE: string = 'YOU DON''T HAVE ';
    s_WAIT: string = '  '#32'R'*'EST';
    s_MOVE: string = 'M'*'OVE';
    s_LEFT: string = ' '#32'L'*'EFT';
    s_RIGHT: string = 'R'*'IGHT';
    s_UP: string = 'U'*'P';
    s_DOWN: string = 'D'*'OWN';
    s_DROPPED: string = 'SO YOU DROPPED IT.';
    s_USED: string = 'YOU''VE USED ';
    s_ATTACKED: string = 'ATTACKED';
    s_DOOR_OPENED: string = 'THE DOOR IS OPEN, ENTERING.';
    s_DOOR_CLOSED: string = 'DOOR IS CLOSED, ';
    s_KEY: string = 'KEY';
    s_BYKEY: string = 'KEY';
    s_WALL: string = 'THIN WALL, ';
    s_HAMMER: string = 'HAMMER';
    s_BYHAMMER: string = 'HAMMER';
    s_ROOM_DARK: string = 'THE ROOM IS DARK,';
    s_LANTERN: string = 'LANTERN';
    s_BYLANTERN: string = 'LANTERN';
    s_ROOM_HOLE: string = 'THERE IS NO FLOOR IN THIS ROOM,';
    s_PLANK: string = 'PLANK';
    s_BYPLANK: string = 'PLANK';
    s_EXIT_PASS: string = 'YOU''VE REACHED AN EXIT. SAY PASSWORD.';
    s_EXIT_PAY: string = 'THANK YOU! GIVE ME A MONEY (100$).';
    s_EXIT_LEAVE: string = 'YOU LEFT AN OLD MANSION WITH ';
    s_EXIT_SCORE: string = ' TO SELL. SCORE=';
    s_EXIT_POOR: string = 'OUCH! YOU DON''T HAVE ENOUGH MONEY.';
    s_EXIT_FATAL: string = 'GUARDIAN CUTS YOUR HEAD OFF.';
    s_EXIT_WRONG_PASS: string = 'WRONG! PASSWORD WAS: ';
    s_BUMP: string = 'YOU HIT THE WALL';
    s_NO_PASARAN: string = 'BETTER FIND OTHER WAY.';
    s_BACK_TO_START: string = 'YOU''VE BEEN TELEPORTED TO START.';
    s_ITEM_BROKE: array [0..7] of string = (
        'HAMMER IS BROKEN, ',
        'LANTERN IS OUT OF OIL, ',
        'KEY IS RUSTED, ',
        'PLANK DECAYED, ',
        'FOOD IS ROTTEN, ',
        'DRINK DRIED OFF, ',
        'BANDAGE IS RIPPED, ',
        'MEDKIT DETERIORATED, '
    );
    s_BROKE: string = 'YOU''VE BROKE ';
    s_YOU_M: string = ' BY ';
    s_YOU_F: string = '';
    s_MONSTER_STR: string = 'ATTACK ';
    s_TOO_WEAK_POOR: string = 'YOU ARE TOO WEAK AND POOR';
    s_TOO_WEAK: string = 'YOU ARE TOO WEAK, YOU MUST PAY.';
    s_TOO_POOR: string = 'YOU HAVE TO FIGHT.';
    s_FIGHT: string = 'F'*'IGHT';
    s_RANSOM: string = 'B'*'RIBE';
    s_HAS_BEEN: string = ' HAS BEEN';
    s_DEFEATED_F: string = '';
    s_DEFEATED_M: string = ' DEFEATED, ';
    s_EARNED: string = 'YOU EARN ';
    s_HAS_STR: string = ' HAS ATTACK ';
    s_WANNA_USE: string = 'DO YOU WANT TO USE AN ITEM (Y/N)?';
    s_WHICH: string = 'WHICH ONE?       ';
    s_CAN_USE_ONLY: string = 'NOW YOU CAN USE ONLY ';
    s_AND: string = 'AND ';
    
    
function needPostfix(monster: byte): boolean;
begin
    result := false; 
end;

procedure ManualPage1;
begin
    Writeln;
    Writeln;
    Writeln(' Znalaz'#123'e'#10' si'#20' nagle w starym domu,   z kt'#16'rego musisz si'#20' wydosta'#22'.');
    Writeln('Jest tu tylko jedno wyj'#10'cie i jest    ono strze'#21'one. Stra'#21'nik wypu'#10'ci ci'#20',');
    Writeln('je'#10'li podasz has'#123'o i zap'#123'acisz 100 $.');
    Writeln;
    Writeln(' Musisz zbiera',#22,' pieni',#8,'dze (w formie');
    Writeln('skarb'#16'w), kt'#16're s'#8' w'#123'asno'#10'ci'#8' potwo-  r'#16'w zamieszkuj'#8'cych dom. Mo'#21'esz z');
    Writeln('nimi walczy'#22', je'#10'li masz mniej ni'#21'    5 ran i si'#123''#20' wi'#20'ksz'#8' od zera.');
    Writeln;
    Writeln(' Mo'#21'esz zwi'#20'kszy'#22' swoj'#8' energi'#20', gdy  znajdziesz lub kupisz prowiant lub');
    Writeln('napoje. Rany mo'#21'esz leczy'#22' '#123'ubkami    lub banda'#21'ami.');
end;
    
procedure ManualPage2;
begin
    Writeln;
    Writeln(' Je'#21'eli nie mo'#21'esz walczy'#22', to musisz zap'#123'aci'#22' potworowi za wypuszczenie.');
    Writeln;
    Writeln(' Has'#123'o znajdziesz w domu. Musisz je   dobrze zapami'#20'ta'#22'.');
    Writeln;
    Writeln;
    Writeln(' Dom jest bardzo stary. W niekt'#16'rych  pokojach jest ciemno, w innych nie');
    Writeln('ma pod'#123'ogi. Mo',#21,'esz tak'#21'e napotka'#22'');
    Writeln('zamkni'#20'te drzwi lub cienkie '#10'cianki.');
    Writeln('Dlatego zbieraj napotkane przedmioty, ale pami'#20'taj, '#21'e mo'#21'esz jednocze'#10'nie');
    Writeln('nie'#10#22' tylko cztery przedmioty i jed-  n'#8' bro'#13'. Wybieraj zawsze najlepsz'#8'');
    Writeln('bro'#13'; zale'#21'y od tego twoja si'#123'a oraz  wielko'#10#22' zdobywanych skarb'#16'w.');
end;
    
procedure ManualPage3;
begin
    Writeln;
    Writeln('           SKR'#15'TY:');
    Writeln;
    Print(char(TILE_EXIT),char(TILE_EXIT2)); Writeln(' - wyj'#10'cie');
    Print(char(TILE_DARK)); Writeln(' - ciemno');
    Print(char(TILE_HOLE)); Writeln(' - brak pod'#123'ogi');
    Print(char(TILE_DOOR_H), #32); Print(char(TILE_DOOR_V)); Writeln( ' - zamkni'#20'te drzwi');
    Print(char(TILE_WALL_H), #32); Print(char(TILE_WALL_V)); Writeln( ' - cienka '#10'cianka');
    Writeln('M - m'#123'otek');
    Writeln('G - kaganek');
    Writeln('K - klucz');
    Writeln('D - deska');
    Writeln('B - banda'#21'e');
    Writeln('L - '#123'ubki');
    Writeln(TILE_PLAYER,' - a to jeste'#10' TY');
end;
